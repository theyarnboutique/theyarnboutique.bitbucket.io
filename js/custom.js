var myOptions = {
    autoplay: true,
    autoplaySpeed: 5000,
    dots:true,
    prevArrow:'<i class="fa fa-chevron-left slick-prev" aria-hidden="true"></i>',
    nextArrow:'<i class="fa fa-chevron-right slick-next" aria-hidden="true"></i>',

}

$('.mySlider').slick(myOptions);

var timeline = gsap.timeline({reapt: 2, repeatDelay: 1});
timeline.from(".yarnimage-fade",{scale: 0.5, y: 50, opacity: 0, duration: 1})
timeline.from(".header h1 span",{stagger: 0.2, y: 20, opacity: 0, duration: 1})
timeline.from(".header li",{stagger: 0.4, scale: 0.4, opacity: 0, duration: 1, ease: "elastic.out(1, 0.5)"})

//Note: I put in the Javscript code back as you told me to in the last class, but I still can't figure out how to use Javascript to get the 
//filters working correctly. As an experiment, the current checkboxfunction will hide the white yarn image when shop All is ticked but the words still appear and 
//if I write the code out for each checkbox filter, I believe the images will only "hide" but not rearrange themselves neatly on the page. 
function checkboxFunction () {
    var checkBoxShopAll = document.getElementById("shop-all"); 
    var whiteYarn = document.getElementById("white-yarn")

    if(checkBoxShopAll.checked == true) {
        whiteYarn.style.display = "none";
    }
    else {
        whiteYarn.style.display = "block";
    }       
}